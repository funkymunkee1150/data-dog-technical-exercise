#! /usr/bin/env bash

# Log simulator
#
# This is a driver used to create log entries and a running process
# that can be monitored.
# 
# Positional Arguments
# --------------------
# output_dir - The first positional arugment should contain the fully
#              qualified path to the output directory.

if [[ ! -d $1 ]];then
  echo "The provided output directory $1 does not exists! Exiting script."
  exit 1 
else
  logfile="$1/logdriver.log"
fi

echo "Starting log driver" > $logfile

while true;do
  timestamp=`date -u +"%FT%TZ"`
  echo "$timestamp - INFO - Log driver running" >> $logfile
  sleep 10
done
